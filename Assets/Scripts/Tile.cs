﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class Tile : Selectable, IPointerClickHandler
{
    public GameObject number_text;
    public GameObject tile_bg;
    
    private int index_number = -1;
    private int currect_number = 0;
    private bool is_default = false;
    private bool is_complete = false;
    private bool selected = false;
    
    public bool isSelected()
    {
        return selected;
    }

    public bool isComplete()
    {
        return is_complete;
    }
    
    public void SetDefaultNumber(int num)
    {
        var item = number_text.GetComponent<Text>();
        if (num <= 0) item.text = " ";
        else
        {
            item.text = num.ToString();
            is_default = true;
            is_complete = true;
            item.color = Color.gray;
        }
    }

    private void SetNumber(int num)
    {
        var item = number_text.GetComponent<Text>();
        if (num <= 0) item.text = " ";
        else item.text = num.ToString();

        is_complete = num == currect_number;
    }

    public void SetSquareIndex(int index)
    {
        index_number = index;
    }

    public void SetCurrectNumber(int number)
    {
        currect_number = number;
    }

    public GameObject GetTileBg()
    {
        return tile_bg;
    }
    
    public void OnPointerClick(PointerEventData eventData)
    {
        selected = true;
        GameEvents.SquareSelectedMethod(index_number);
    }
    
    public void OnSetNumber(int number)
    {
        if (is_default || !selected) return;
        SetNumber(number);
        
        if (number == currect_number)
        {
            tile_bg.GetComponent<Image>().color = Color.white;
            number_text.GetComponent<Text>().color = Color.green;
        }
        else
        {
            tile_bg.GetComponent<Image>().color = Color.red;
            number_text.GetComponent<Text>().color = Color.red;
        }
        
        GameEvents.CheckLevelCompleteMethod();
    }
    
    public void OnSquareSelectedNum(int selected_index)
    {
        if (selected_index != index_number) selected = false;
        GameEvents.HighliteRelatedSquaersMethod(selected_index);
    }
    
    void OnEnable()
    {
        GameEvents.OnUpdateSquareNumber += OnSetNumber;
        GameEvents.OnSquareSelected += OnSquareSelectedNum;
    }

    void OnDisable()
    {
        GameEvents.OnUpdateSquareNumber -= OnSetNumber;
        GameEvents.OnSquareSelected -= OnSquareSelectedNum;
    }
}
