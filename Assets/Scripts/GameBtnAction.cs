﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameBtnAction : MonoBehaviour
{
    public void EasyBtn(string scene)
    {
        Debug.Log("easy");
        GameLevel.Get.SetLevel(GameLevel.easy);
        SceneManager.LoadScene(scene);
    }

    public void MediumBtn(string scene)
    {
        Debug.Log("medium");
        GameLevel.Get.SetLevel(GameLevel.medium);
        SceneManager.LoadScene(scene);
    }

    public void HardBtn(string scene)
    {
        Debug.Log("hard");
        GameLevel.Get.SetLevel(GameLevel.hard);
        SceneManager.LoadScene(scene);
    }
}
