﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class NumberButtons : MonoBehaviour, IPointerClickHandler
{
    public int value = 0;
    
    public void OnPointerClick(PointerEventData eventData)
    {
        GameEvents.OnUpdateSquareNumber(value);
    }
}
