﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLevel : MonoBehaviour
{
    public static GameLevel Get;

    public static string easy = "easy";
    public static string medium = "medium";
    public static string hard = "hard";

    private string game_level = easy;
    
    void Start()
    {
        if (Get == null)
        {
            DontDestroyOnLoad(this);
            Get = this;
        }
        else
        {
            Destroy(this);
        }
    }

    public void SetLevel(string level)
    {
        game_level = level;
    }

    public string GetLevel()
    {
        return game_level;
    }

    public int GetLevelRange()
    {
        if (game_level == medium) return 4;
        else if (game_level == hard) return 2;
        else return 7;
    }
}
