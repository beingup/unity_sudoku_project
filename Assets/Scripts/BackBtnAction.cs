﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackBtnAction : MonoBehaviour
{
    public GameObject back_btn;

    public void BackBtn(string scene)
    {
        SceneManager.LoadScene(scene);
    }
}
