﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameEvents : MonoBehaviour
{
	// Tile callbacks
    public delegate void UpdateSquareNumber(int num);
    public static UpdateSquareNumber OnUpdateSquareNumber;
	
	public static void UpdateSquareNumberMethod(int num)
	{
		if (OnUpdateSquareNumber != null) OnUpdateSquareNumber(num);
	}
	
	public delegate void SquareSelected(int index_number);
	public static event SquareSelected OnSquareSelected;

	public static void SquareSelectedMethod(int index)
	{
		if (OnSquareSelected != null) OnSquareSelected(index);
	}

	// Board activity callbacks
	public delegate void CheckLevelComplete();
	public static event CheckLevelComplete OnCheckLevelComplete;

	public static void CheckLevelCompleteMethod()
	{
		if (OnCheckLevelComplete != null) OnCheckLevelComplete();
	}

	public delegate void HighliteRelatedSquaers(int index_number);
	public static event HighliteRelatedSquaers OnHighliteRelatedSquaers;

	public static void HighliteRelatedSquaersMethod(int index)
	{
		if (OnHighliteRelatedSquaers != null) OnHighliteRelatedSquaers(index);
	}
}
